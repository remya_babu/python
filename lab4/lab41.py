from datetime import date
print(date.today())   #date module

import time
print(time.time())        #time module

import datetime
a=datetime.datetime.now()  #date time module
print(a)

import calendar
print(calendar.calendar(2022)) #calendar module

import math
n=int(input("Enter a number: "))                       #math module
print("square root of",n,"is: ",math.sqrt(n))
print("square root of",n,"is: ",math.sin(n))
print("square root of",n,"is: ",math.tan(n))
print("square root of",n,"is: ",math.cos(n))

from math import pi
n=int(input("Enter a number: "))
print("area of a circle with radius",n,"is: ",pi*n*n) # import value of pi from math module

import string
print(string.ascii_letters)       #string modules
print(string.ascii_lowercase)
print(string.ascii_uppercase)
print(string.digits)
print(string.whitespace)
