numberToCheck=int(input("Enter the number to be checked\n"))

# Checking whether the number is even or not
if numberToCheck%2==0:
    # If yes, printing the message : "The number is even"
    print("Number is even")
else:
    # If no, printing the message : "The number is not even"
    print("Number is not even")
