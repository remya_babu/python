str = input("Enter the string to be encrypted: ")
key = int(input("Enter the key value: "))

# Function to encrypt the string
def encrypt(str, key):
    encrypted_str = "" # Initializing encrypted string
    for i in range(len(str)): # Looping through the string
        encrypted_str += chr(ord(str[i]) + key) # Adding key value to each character
    return encrypted_str

print("Encrypted string:", encrypt(str,key)) # Printing encrypted string
